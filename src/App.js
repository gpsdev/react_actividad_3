import "./App.css";
import { Botoncontador } from "./components/Botoncontador";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Botoncontador />
      </header>
    </div>
  );
}

export default App;
